class CreateCharacters < ActiveRecord::Migration[5.1]
  def change
    create_table :characters do |t|
      t.references :owner,      null: false, index: true
      t.references :game,       null: false, foreign_key: true, index: true
      t.text :name,             null: false
      t.text :description,      null: false
      t.text :action,           null: false, default: ""
      t.boolean :star,          null: false, default: false
      t.integer :status,        null: false, default: 0
      t.text :roll,             null: true

      t.timestamps
    end

    add_index :characters, [:name, :game_id], unique: true
    add_foreign_key :characters, :users, column: :owner_id
  end
end
