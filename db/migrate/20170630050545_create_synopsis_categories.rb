class CreateSynopsisCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :synopsis_categories do |t|
      t.references :game,  null: false, foreign_key: true
      t.text :name,        null: false

      t.timestamps
    end

    add_index :synopsis_categories, [:game_id, :name], unique: true
  end
end
