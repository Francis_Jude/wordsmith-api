class CreateWords < ActiveRecord::Migration[5.1]
  def change
    create_table :words do |t|
      t.references :character, null: false, foreign_key: true, index: true
      t.text :name,            null: false
      t.boolean :active,       null: false, default: false

      t.timestamps
    end
  end
end
