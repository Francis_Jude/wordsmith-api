class CreateManualEntries < ActiveRecord::Migration[5.1]
  def change
    create_table :manual_entries do |t|
      t.text :name,      null: false
      t.text :content,   null: false

      t.timestamps
    end

    add_index :manual_entries, :name, unique: true
  end
end
