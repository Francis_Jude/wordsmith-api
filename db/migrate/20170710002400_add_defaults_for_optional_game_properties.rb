class AddDefaultsForOptionalGameProperties < ActiveRecord::Migration[5.1]
  def change
    change_column :games, :ready, :boolean, null: false, default: false
    change_column :games, :allow_invites, :boolean, null: false, default: true
    change_column :games, :visible, :boolean, null: false, default: true
  end
end
