class CreateGames < ActiveRecord::Migration[5.1]
  def change
    create_table :games do |t|
      t.text :name,             null: false
      t.text :description,      null: false
      t.text :scene,            null: false
      t.boolean :ready,         null: false
      t.boolean :allow_invites, null: false
      t.boolean :visible,       null: false

      t.references :owner,      null: false, index: true

      t.timestamps
    end

    add_foreign_key :games, :users, column: :owner_id

    create_table :game_invites do |t|
      t.references :user, null: false, foreign_key: true
      t.references :game, null: false, foreign_key: true

      t.timestamps
    end

    add_index :game_invites, [:user_id, :game_id], unique: true

    create_table :game_bans do |t|
      t.references :game, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end

    add_index :game_bans, [:user_id, :game_id], unique: true
  end
end
