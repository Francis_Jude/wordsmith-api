class CreateHistoryItems < ActiveRecord::Migration[5.1]
  def change
    create_table :history_items do |t|
      t.references :game,  null: false, index: true, foreign_key: true
      t.datetime :date,    null: false, index: true
      t.text :actions,     null: false

      t.timestamps
    end
  end
end
