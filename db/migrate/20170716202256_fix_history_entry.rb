class FixHistoryEntry < ActiveRecord::Migration[5.1]
  def self.up
    remove_index :history_items, :game_id
    remove_index :history_items, :date
    rename_table :history_items, :history_entries
    add_column :history_entries, :scene, :text, null: false, default: ""
    change_column_default :history_entries, :scene, nil

    add_index :history_entries, [:game_id, :date], unique: true
  end

  def self.down
    remove_index :history_entries, [:game_id, :date]
    remove_column :history_entries, :scene
    rename_table :history_entries, :history_items
    add_index :history_items, :game_id
    add_index :history_items, :date
  end
end
