class CreateSynopsisEntries < ActiveRecord::Migration[5.1]
  def change
    create_table :synopsis_entries do |t|
      t.references :synopsis_category, null: false, foreign_key: true
      t.text :name,                    null: false
      t.text :description,             null: false

      t.timestamps
    end

    add_index :synopsis_entries, [:synopsis_category_id, :name], unique: true
  end
end
