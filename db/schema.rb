# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170716202256) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "characters", force: :cascade do |t|
    t.bigint "owner_id", null: false
    t.bigint "game_id", null: false
    t.text "name", null: false
    t.text "description", null: false
    t.text "action", default: "", null: false
    t.boolean "star", default: false, null: false
    t.integer "status", default: 0, null: false
    t.text "roll"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["game_id"], name: "index_characters_on_game_id"
    t.index ["name", "game_id"], name: "index_characters_on_name_and_game_id", unique: true
    t.index ["owner_id"], name: "index_characters_on_owner_id"
  end

  create_table "game_bans", force: :cascade do |t|
    t.bigint "game_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["game_id"], name: "index_game_bans_on_game_id"
    t.index ["user_id", "game_id"], name: "index_game_bans_on_user_id_and_game_id", unique: true
    t.index ["user_id"], name: "index_game_bans_on_user_id"
  end

  create_table "game_invites", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "game_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["game_id"], name: "index_game_invites_on_game_id"
    t.index ["user_id", "game_id"], name: "index_game_invites_on_user_id_and_game_id", unique: true
    t.index ["user_id"], name: "index_game_invites_on_user_id"
  end

  create_table "games", force: :cascade do |t|
    t.text "name", null: false
    t.text "description", null: false
    t.text "scene", null: false
    t.boolean "ready", default: false, null: false
    t.boolean "allow_invites", default: true, null: false
    t.boolean "visible", default: true, null: false
    t.bigint "owner_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["owner_id"], name: "index_games_on_owner_id"
  end

  create_table "history_entries", force: :cascade do |t|
    t.bigint "game_id", null: false
    t.datetime "date", null: false
    t.text "actions", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "scene", null: false
    t.index ["game_id", "date"], name: "index_history_entries_on_game_id_and_date", unique: true
  end

  create_table "items", force: :cascade do |t|
    t.bigint "character_id", null: false
    t.text "name", null: false
    t.text "description", null: false
    t.boolean "active", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["character_id"], name: "index_items_on_character_id"
  end

  create_table "manual_entries", force: :cascade do |t|
    t.text "name", null: false
    t.text "content", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_manual_entries_on_name", unique: true
  end

  create_table "synopsis_categories", force: :cascade do |t|
    t.bigint "game_id", null: false
    t.text "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["game_id", "name"], name: "index_synopsis_categories_on_game_id_and_name", unique: true
    t.index ["game_id"], name: "index_synopsis_categories_on_game_id"
  end

  create_table "synopsis_entries", force: :cascade do |t|
    t.bigint "synopsis_category_id", null: false
    t.text "name", null: false
    t.text "description", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["synopsis_category_id", "name"], name: "index_synopsis_entries_on_synopsis_category_id_and_name", unique: true
    t.index ["synopsis_category_id"], name: "index_synopsis_entries_on_synopsis_category_id"
  end

  create_table "users", force: :cascade do |t|
    t.text "name", null: false
    t.text "display_name", null: false
    t.text "email", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["display_name"], name: "index_users_on_display_name", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  create_table "words", force: :cascade do |t|
    t.bigint "character_id", null: false
    t.text "name", null: false
    t.boolean "active", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["character_id"], name: "index_words_on_character_id"
  end

  add_foreign_key "characters", "games"
  add_foreign_key "characters", "users", column: "owner_id"
  add_foreign_key "game_bans", "games"
  add_foreign_key "game_bans", "users"
  add_foreign_key "game_invites", "games"
  add_foreign_key "game_invites", "users"
  add_foreign_key "games", "users", column: "owner_id"
  add_foreign_key "history_entries", "games"
  add_foreign_key "items", "characters"
  add_foreign_key "synopsis_categories", "games"
  add_foreign_key "synopsis_entries", "synopsis_categories"
  add_foreign_key "words", "characters"
end
