# == Schema Information
#
# Table name: words
#
#  id           :integer          not null, primary key
#  character_id :integer          not null
#  name         :text             not null
#  active       :boolean          default(FALSE), not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

require 'rails_helper'

RSpec.describe Word, type: :model do
  it { should validate_presence_of(:name) }

  describe "validations" do
    subject { create :word }
    it { should validate_uniqueness_of(:name).scoped_to(:character_id) }
  end

  it { should belong_to(:character) }
end
