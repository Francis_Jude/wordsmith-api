# == Schema Information
#
# Table name: manual_entries
#
#  id         :integer          not null, primary key
#  name       :text             not null
#  content    :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe ManualEntry, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:content) }

  describe "validations" do
    subject { create :manual_entry }
    it { should validate_uniqueness_of(:name) }
  end
end
