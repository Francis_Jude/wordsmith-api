# == Schema Information
#
# Table name: users
#
#  id           :integer          not null, primary key
#  name         :text             not null
#  display_name :text             not null
#  email        :text             not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

require 'rails_helper'

RSpec.describe User, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:display_name) }
  it { should validate_presence_of(:email) }

  describe "validations" do
    subject { create :user }
    it { should validate_uniqueness_of(:display_name) }
    it { should validate_uniqueness_of(:email) }
  end

  it { should have_many(:owned_games).inverse_of('owner').with_foreign_key('owner_id') }
  it { should have_many(:game_invites).inverse_of(:user) }
  it { should have_many(:game_bans).inverse_of(:user) }
  it { should have_many(:games_invited_to).through(:game_invites).source(:game) }
  it { should have_many(:banned_games).through(:game_bans).source(:game) }
  it { should have_many(:characters).inverse_of('owner').with_foreign_key('owner_id') }
end
