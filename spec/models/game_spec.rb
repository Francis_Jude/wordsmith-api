# == Schema Information
#
# Table name: games
#
#  id            :integer          not null, primary key
#  name          :text             not null
#  description   :text             not null
#  scene         :text             not null
#  ready         :boolean          default(FALSE), not null
#  allow_invites :boolean          default(TRUE), not null
#  visible       :boolean          default(TRUE), not null
#  owner_id      :integer          not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'rails_helper'

RSpec.describe Game, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:description) }
  it { should validate_presence_of(:scene) }

  it { should belong_to(:owner).class_name('User') }
  it { should have_many(:game_invites).inverse_of(:game) }
  it { should have_many(:game_bans).inverse_of(:game) }
  it { should have_many(:invited_users).through(:game_invites).source(:user) }
  it { should have_many(:banned_users).through(:game_bans).source(:user) }
  it { should have_many(:characters).inverse_of(:game) }
  it { should have_many(:history_entries).inverse_of(:game) }
end
