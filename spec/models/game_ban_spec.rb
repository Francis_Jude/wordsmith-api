# == Schema Information
#
# Table name: game_bans
#
#  id         :integer          not null, primary key
#  game_id    :integer          not null
#  user_id    :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe GameBan, type: :model do
  describe "validations" do
    subject { create :game_ban }
    it { should validate_uniqueness_of(:user).scoped_to(:game_id) }
  end

  it { should belong_to(:user) }
  it { should belong_to(:game) }
end
