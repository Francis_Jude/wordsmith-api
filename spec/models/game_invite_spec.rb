# == Schema Information
#
# Table name: game_invites
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  game_id    :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe GameInvite, type: :model do
  describe "validations" do
    subject { create :game_invite }
    it { should validate_uniqueness_of(:user).scoped_to(:game_id) }
  end

  it { should belong_to(:user) }
  it { should belong_to(:game) }
end
