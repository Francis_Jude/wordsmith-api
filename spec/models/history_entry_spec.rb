# == Schema Information
#
# Table name: history_entries
#
#  id         :integer          not null, primary key
#  game_id    :integer          not null
#  date       :datetime         not null
#  actions    :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  scene      :text             not null
#

require 'rails_helper'

RSpec.describe HistoryEntry, type: :model do
  it { should validate_presence_of(:date) }
  it { should validate_presence_of(:scene) }
  it { should validate_presence_of(:actions) }

  describe "validations" do
    subject { create :history_entry }
    it { should validate_uniqueness_of(:date).scoped_to(:game_id) }
  end

  it { should belong_to(:game) }
end
