# == Schema Information
#
# Table name: synopsis_categories
#
#  id         :integer          not null, primary key
#  game_id    :integer          not null
#  name       :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe SynopsisCategory, type: :model do
  it { should validate_presence_of(:name) }

  describe "validations" do
    subject { create :synopsis_category }
    it { should validate_uniqueness_of(:name).scoped_to(:game_id) }
  end

  it { should belong_to(:game) }
  it { should have_many(:synopsis_entries).inverse_of(:synopsis_category) }
end
