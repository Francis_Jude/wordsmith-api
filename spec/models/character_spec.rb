# == Schema Information
#
# Table name: characters
#
#  id          :integer          not null, primary key
#  owner_id    :integer          not null
#  game_id     :integer          not null
#  name        :text             not null
#  description :text             not null
#  action      :text             default(""), not null
#  star        :boolean          default(FALSE), not null
#  status      :integer          default("waiting"), not null
#  roll        :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe Character, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:description) }

  describe "validations" do
    subject { create :character }
    it { should validate_uniqueness_of(:owner).scoped_to(:game_id) }
    it { should validate_uniqueness_of(:name).scoped_to(:game_id) }
  end

  it { should belong_to(:owner).class_name('User') }
  it { should belong_to(:game) }
  it { should have_many(:words).inverse_of(:character) }
  it { should have_many(:items).inverse_of(:character) }
end
