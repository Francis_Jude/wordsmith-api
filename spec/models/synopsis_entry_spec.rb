# == Schema Information
#
# Table name: synopsis_entries
#
#  id                   :integer          not null, primary key
#  synopsis_category_id :integer          not null
#  name                 :text             not null
#  description          :text             not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

require 'rails_helper'

RSpec.describe SynopsisEntry, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:description) }

  it { should belong_to(:synopsis_category) }
end
