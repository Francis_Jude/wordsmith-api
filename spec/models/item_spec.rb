# == Schema Information
#
# Table name: items
#
#  id           :integer          not null, primary key
#  character_id :integer          not null
#  name         :text             not null
#  description  :text             not null
#  active       :boolean          default(FALSE), not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

require 'rails_helper'

RSpec.describe Item, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:description) }

  it { should belong_to(:character) }
end
