require 'rails_helper'

RSpec.describe 'ManualEntries controller', type: :request do

  describe 'GET /api/v1/manual_entries' do
    let!(:manual_entry1) { create :manual_entry }
    let!(:manual_entry2) { create :manual_entry }

    it 'returns a list of all manual_entries' do
      get '/api/v1/manual_entries'
      expect(response).to be_success
      expect(json.size).to eq(2)
      assert_response_schema
    end
  end

  describe 'GET /api/v1/manual_entries/:id' do

    let!(:manual_entry) { create :manual_entry }

    context 'when the given manual_entry id exists' do
      before do
        get "/api/v1/manual_entries/#{manual_entry.id}"
      end

      it 'returns the manual_entry data in the expected schema' do
        expect(response).to be_success
        assert_response_schema
      end
    end

    context "when the given manual_entry id doesn't exist" do
      before do
        get "/api/v1/manual_entries/#{manual_entry.id + 1}"
      end

      it 'returns a Not Found repsonse' do
        expect(response).to have_http_status(:not_found)
      end
    end
  end

  describe 'POST /api/v1/manual_entry' do

    let(:owner) { create :user }

    context 'when the input is valid' do

      before do
        params = {
          name: "Getting Started",
          content: 'Just click Play',
        }

        post '/api/v1/manual_entries', params: params
      end

      it "returns a Created response with the new ManualEntry model's ID" do
        expect(response).to have_http_status(:created)
        expect(ManualEntry.count).to eq(1)
        new_model = ManualEntry.first
        expect(json).to eq( 'id' => new_model.id )
      end

      it 'saves a new ManualEntry model in the database' do
        expect(ManualEntry.count).to eq(1)
        new_model = ManualEntry.first
        expect(new_model.name).to eq("Getting Started")
        expect(new_model.content).to eq('Just click Play')
      end
    end

    context 'when required parameters are missing' do
      before do
        params = {}

        post '/api/v1/manual_entries', params: params
      end

      it 'returns a Bad Request response with the error message' do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          'errors' => {
            'name' => ["can't be blank"],
            'content' => ["can't be blank"],
          }
        )
      end

      it "doesn't save a new ManualEntry model" do
        expect(ManualEntry.count).to eq(0)
      end
    end
  end

  describe 'PUT/PATCH /api/v1/manual_entries/:id' do

    let!(:manual_entry) do
      create(
        :manual_entry,
        {
          name: "Getting Started",
          content: 'Just click Play',
        }
      )
    end

    context 'when the input is valid' do

      before do
        params = {
          name: "Step Two",
          content: "Go to step one.",
        }

        put "/api/v1/manual_entries/#{manual_entry.id}", params: params
      end

      it "returns a successful response" do
        expect(response).to have_http_status(:no_content)
      end

      it 'updates the ManualEntry model in the database' do
        expect(ManualEntry.count).to eq(1)
        model = ManualEntry.first
        expect(model.name).to eq("Step Two")
        expect(model.content).to eq("Go to step one.")
      end
    end

    context "when the given id doesn't exist" do

      before do
        params = {}

        put "/api/v1/manual_entries/#{manual_entry.id + 1}", params: params
      end

      it "returns a Not Found response" do
        expect(response).to have_http_status(:not_found)
      end
    end

    context "when an id parameter is passed" do

      before do
        params = {
          id: -1,
          name: "Getting Started",
          content: 'Just click Play',
        }

        put "/api/v1/manual_entries/#{manual_entry.id}", params: params
      end

      it 'ignores the id parameter' do
        expect(response).to have_http_status(:no_content)
        expect(manual_entry.id).not_to eq(-1)
      end
    end
  end

  describe 'DELETE /api/v1/manual_entries/:id' do

    let!(:manual_entry) do
      create(
        :manual_entry,
        {
          name: "Getting Started",
          content: 'Just click Play',
        }
      )
    end

    context 'when the given manual_entry id exists' do
      before do
        delete "/api/v1/manual_entries/#{manual_entry.id}"
      end

      it "returns a successful response" do
        expect(response).to have_http_status(:no_content)
      end

      it 'removes the ManualEntry model from the database' do
        expect(ManualEntry.count).to eq(0)
      end
    end

    context "when the given id doesn't exist" do
      before do
        put "/api/v1/manual_entries/#{manual_entry.id + 1}"
      end

      it "returns a Not Found response" do
        expect(response).to have_http_status(:not_found)
      end
    end
  end

end
