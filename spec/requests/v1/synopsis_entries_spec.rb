require 'rails_helper'

RSpec.describe 'SynopsisEntries controller', type: :request do

  describe 'GET /api/v1/synopsis_categories/:synopsis_category_id/synopsis_entries' do
    let!(:synopsis_category) { create :synopsis_category }
    let!(:synopsis_entry1) do
      create(
        :synopsis_entry,
        {
          name: "Seacliff",
          description: "A breezy cliff by the beach.",
          synopsis_category: synopsis_category,
        }
      )
    end
    let!(:synopsis_entry2) do
      create(
        :synopsis_entry,
        {
          name: "Rock Creek",
          description: "The bubbly creek that passes through Harborstone.",
          synopsis_category: synopsis_category,
        }
      )
    end

    let!(:other_synopsis_category) { create :synopsis_category }
    let!(:irrelevant_synopsis_entry) do
      create :synopsis_entry, synopsis_category: other_synopsis_category
    end

    it 'returns a list of all synopsis_entries belonging to the given synopsis_category' do
      get "/api/v1/synopsis_categories/#{synopsis_category.id}/synopsis_entries"
      expect(response).to be_success
      expect(json.size).to eq(2)
      assert_response_schema
    end
  end

  describe 'GET /api/v1/synopsis_entries/:id' do

    let!(:synopsis_entry) { create :synopsis_entry }

    context 'when the given synopsis_entry id exists' do
      before do
        get "/api/v1/synopsis_entries/#{synopsis_entry.id}"
      end

      it 'returns the synopsis_entry data in the expected schema' do
        expect(response).to be_success
        assert_response_schema
      end
    end

    context "when the given synopsis_entry id doesn't exist" do
      before do
        get "/api/v1/synopsis_entries/#{synopsis_entry.id + 1}"
      end

      it 'returns a Not Found repsonse' do
        expect(response).to have_http_status(:not_found)
      end
    end
  end

  describe 'POST /api/v1/synopsis_categories/:synopsis_category_id/synopsis_entry' do
    context 'when the input is valid' do
      let!(:synopsis_category) { create :synopsis_category }

      before do
        params = {
          name: "Seacliff",
          description: "A breezy cliff by the beach.",
        }

        post "/api/v1/synopsis_categories/#{synopsis_category.id}/synopsis_entries", params: params
      end

      it "returns a Created response with the new SynopsisEntry model's ID" do
        expect(response).to have_http_status(:created)
        expect(SynopsisEntry.count).to eq(1)
        new_model = SynopsisEntry.first
        expect(json).to eq( 'id' => new_model.id )
      end

      it 'saves a new SynopsisEntry model in the database' do
        expect(SynopsisEntry.count).to eq(1)
        new_model = SynopsisEntry.first
        expect(new_model.name).to eq("Seacliff")
        expect(new_model.description).to eq("A breezy cliff by the beach.")
        expect(new_model.synopsis_category).to eq(synopsis_category)
      end
    end

    context "when the input violates the model's unique contraints" do
      let!(:synopsis_category) { create :synopsis_category }
      let!(:existing_synopsis_entry) do
        create :synopsis_entry, name: "Seacliff", synopsis_category: synopsis_category
      end

      before do
        params = {
          name: "Seacliff",
          description: "A breezy cliff by the beach.",
        }

        post "/api/v1/synopsis_categories/#{synopsis_category.id}/synopsis_entries", params: params
      end

      it 'returns a Bad Request response with the error message' do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          'errors' => {
            'name' => ['has already been taken'],
          }
        )
      end

      it "doesn't save a new SynopsisEntry model" do
        expect(SynopsisEntry.count).to eq(1)
      end
    end

    context 'when required parameters are missing' do
      let!(:synopsis_category) { create :synopsis_category }

      before do
        params = {}

        post "/api/v1/synopsis_categories/#{synopsis_category.id}/synopsis_entries", params: params
      end

      it 'returns a Bad Request response with the error message' do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          'errors' => {
            'name' => ["can't be blank"],
            'description' => ["can't be blank"],
          }
        )
      end

      it "doesn't save a new SynopsisEntry model" do
        expect(SynopsisEntry.count).to eq(0)
      end
    end

    context "when an id parameter is passed" do
      let!(:synopsis_category) { create :synopsis_category }

      before do
        params = {
          id: -1,
          name: "Seacliff",
          description: "A breezy cliff by the beach.",
        }

        post "/api/v1/synopsis_categories/#{synopsis_category.id}/synopsis_entries", params: params
      end

      it 'ignores the id parameter' do
        expect(response).to have_http_status(:created)
        expect(SynopsisEntry.count).to eq(1)
        expect(SynopsisEntry.first.id).not_to eq(-1)
      end
    end

    context "when the parent synopsis_category ID doesn't exist" do

      before do
        params = {
          name: "Seacliff",
          description: "A breezy cliff by the beach.",
        }

        post "/api/v1/synopsis_categories/1/synopsis_entries", params: params
      end

      it 'returns a Bad Request response with the error message' do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          "errors" => {
            "synopsis_category" => ["must exist"],
          }
        )
      end
    end
  end

  describe 'PUT/PATCH /api/v1/synopsis_entries/:id' do

    let!(:synopsis_category) { create :synopsis_category }
    let!(:synopsis_entry) do
      create(
        :synopsis_entry,
        {
          name: "Seacliff",
          description: "A breezy cliff by the beach.",
          synopsis_category: synopsis_category,
        }
      )
    end

    context 'when the input is valid' do

      let!(:new_synopsis_category) { create :synopsis_category }

      before do
        params = {
          name: "Rock Creek",
          description: "The bubbly creek that passes through Harborstone.",
          synopsis_category_id: new_synopsis_category.id,
        }

        put "/api/v1/synopsis_entries/#{synopsis_entry.id}", params: params
      end

      it "returns a successful response" do
        expect(response).to have_http_status(:no_content)
      end

      it 'updates the SynopsisEntry model in the database' do
        expect(SynopsisEntry.count).to eq(1)
        model = SynopsisEntry.first
        expect(model.name).to eq("Rock Creek")
        expect(model.description).to eq("The bubbly creek that passes through Harborstone.")
        expect(model.synopsis_category).to eq(new_synopsis_category)
      end
    end

    context "when the given SynopsisEntry ID doesn't exist" do
      before do
        params = {
          name: "Rock Creek",
        }

        put "/api/v1/synopsis_entries/#{synopsis_entry.id + 1}", params: params
      end

      it "returns a Not Found response" do
        expect(response).to have_http_status(:not_found)
      end
    end

    context "when the given SynopsisCategory ID doesn't exist" do
      before do
        params = {
          name: "Rock Creek",
          description: "The bubbly creek that passes through Harborstone.",
          synopsis_category_id: synopsis_category.id + 1,
        }

        put "/api/v1/synopsis_entries/#{synopsis_entry.id}", params: params
      end

      it "returns a Bad Request response" do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          "errors" => {
            "synopsis_category" => ["must exist"],
          }
        )
      end

      it "doesn't update the SynopsisEntry model in the database" do
        synopsis_entry.reload
        expect(synopsis_entry.name).to eq("Seacliff")
        expect(synopsis_entry.description).to eq("A breezy cliff by the beach.")
        expect(synopsis_entry.synopsis_category).to eq(synopsis_category)
      end
    end

    context "when the input violates the model's unique contraints" do
      before do
        create :synopsis_entry, name: "Rock Creek", synopsis_category: synopsis_category
        params = {
          name: "Rock Creek",
        }

        put "/api/v1/synopsis_entries/#{synopsis_entry.id}", params: params
      end

      it 'returns a Bad Request response with the error message' do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          "errors" => {
            "name" => ["has already been taken"],
          }
        )
      end

      it "doesn't update the SynopsisEntry model in the database" do
        synopsis_entry.reload
        expect(synopsis_entry.name).to eq("Seacliff")
      end
    end

    context "when an id parameter is passed" do
      before do
        params = {
          id: -1,
          name: "Rock Creek",
        }

        put "/api/v1/synopsis_entries/#{synopsis_entry.id}", params: params
      end

      it 'ignores the id parameter' do
        expect(response).to have_http_status(:no_content)
        expect(synopsis_entry.id).not_to eq(-1)
      end
    end
  end

  describe 'DELETE /api/v1/synopsis_entries/:id' do

    let!(:synopsis_entry) { create :synopsis_entry }

    context 'when the given synopsis_entry id exists' do
      before do
        delete "/api/v1/synopsis_entries/#{synopsis_entry.id}"
      end

      it "returns a successful response" do
        expect(response).to have_http_status(:no_content)
      end

      it 'removes the SynopsisEntry model from the database' do
        expect(SynopsisEntry.count).to eq(0)
      end
    end

    context "when the given id doesn't exist" do
      before do
        put "/api/v1/synopsis_entries/#{synopsis_entry.id + 1}"
      end

      it "returns a Not Found response" do
        expect(response).to have_http_status(:not_found)
      end
    end
  end

end
