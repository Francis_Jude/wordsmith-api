require 'rails_helper'

RSpec.describe 'Synopsis Categories controller', type: :request do

  describe 'GET /api/v1/games/:game_id/synopsis_categories' do
    let!(:game) { create :game }
    let!(:synopsis_category1) { create :synopsis_category, name: "People", game: game }
    let!(:synopsis_category2) { create :synopsis_category, name: "Places", game: game }

    let!(:other_game) { create :game }
    let!(:irrelevant_synopsis_category) { create :synopsis_category, game: other_game }

    it 'returns a list of all synopsis_categories belonging to the given game' do
      get "/api/v1/games/#{game.id}/synopsis_categories"
      expect(response).to be_success
      expect(json.size).to eq(2)
      assert_response_schema
    end
  end

  describe 'GET /api/v1/synopsis_categories/:id' do

    let!(:synopsis_category) { create :synopsis_category }

    context 'when the given synopsis_category id exists' do
      before do
        get "/api/v1/synopsis_categories/#{synopsis_category.id}"
      end

      it 'returns the synopsis_category data in the expected schema' do
        expect(response).to be_success
        assert_response_schema
      end
    end

    context "when the given synopsis_category id doesn't exist" do
      before do
        get "/api/v1/synopsis_categories/#{synopsis_category.id + 1}"
      end

      it 'returns a Not Found repsonse' do
        expect(response).to have_http_status(:not_found)
      end
    end
  end

  describe 'POST /api/v1/games/:game_id/synopsis_category' do
    context 'when the input is valid' do
      context 'and the optional parameters are supplied' do

        let!(:game) { create :game }

        before do
          params = {
            name: "People",
          }

          post "/api/v1/games/#{game.id}/synopsis_categories", params: params
        end

        it "returns a Created response with the new SynopsisCategory model's ID" do
          expect(response).to have_http_status(:created)
          expect(SynopsisCategory.count).to eq(1)
          new_model = SynopsisCategory.first
          expect(json).to eq( 'id' => new_model.id )
        end

        it 'saves a new SynopsisCategory model in the database' do
          expect(SynopsisCategory.count).to eq(1)
          new_model = SynopsisCategory.first
          expect(new_model.name).to eq("People")
          expect(new_model.game).to eq(game)
        end
      end

      context 'and the optional parameters are missing' do

        let!(:game) { create :game }

        before do
          params = {
            name: "People",
          }

          post "/api/v1/games/#{game.id}/synopsis_categories", params: params
        end

        it "returns a Created response with the new SynopsisCategory model's ID" do
          expect(response).to have_http_status(:created)
          expect(SynopsisCategory.count).to eq(1)
          new_model = SynopsisCategory.first
          expect(json).to eq( 'id' => new_model.id )
        end

        it 'sets default values for the optional properties' do
          expect(SynopsisCategory.count).to eq(1)
          new_model = SynopsisCategory.first
        end
      end
    end

    context "when the input violates the model's unique contraints" do
      let!(:existing_synopsis_category) { create :synopsis_category, name: "People" }

      before do
        params = {
          name: "People"
        }

        post "/api/v1/games/#{existing_synopsis_category.game.id}/synopsis_categories", params: params
      end

      it 'returns a Bad Request response with the error message' do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          'errors' => {
            'name' => ['has already been taken'],
          }
        )
      end

      it "doesn't save a new SynopsisCategory model" do
        expect(SynopsisCategory.count).to eq(1)
      end
    end

    context 'when required parameters are missing' do
      let!(:game) { create :game }

      before do
        params = {}

        post "/api/v1/games/#{game.id}/synopsis_categories", params: params
      end

      it 'returns a Bad Request response with the error message' do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          'errors' => {
            'name' => ["can't be blank"],
          }
        )
      end

      it "doesn't save a new SynopsisCategory model" do
        expect(SynopsisCategory.count).to eq(0)
      end
    end

    context "when an id parameter is passed" do
      let!(:game) { create :game }

      before do
        params = {
          id: -1,
          name: "People",
        }

        post "/api/v1/games/#{game.id}/synopsis_categories", params: params
      end

      it 'ignores the id parameter' do
        expect(response).to have_http_status(:created)
        expect(SynopsisCategory.count).to eq(1)
        expect(SynopsisCategory.first.id).not_to eq(-1)
      end
    end

    context "when the parent game ID doesn't exist" do
      before do
        params = {
          name: "People",
        }

        post "/api/v1/games/1/synopsis_categories", params: params
      end

      it 'returns a Bad Request response with the error message' do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          "errors" => {
            "game" => ["must exist"],
          }
        )
      end
    end
  end

  describe 'PUT/PATCH /api/v1/synopsis_categories/:id' do

    let!(:game) { create :game }
    let!(:synopsis_category) { create :synopsis_category, name: "People", game: game }

    context 'when the input is valid' do

      let!(:new_game) { create :game }

      before do
        params = {
          name: "Places",
          game_id: new_game.id,
        }

        put "/api/v1/synopsis_categories/#{synopsis_category.id}", params: params
      end

      it "returns a successful response" do
        expect(response).to have_http_status(:no_content)
      end

      it 'updates the SynopsisCategory model in the database' do
        expect(SynopsisCategory.count).to eq(1)
        model = SynopsisCategory.first
        expect(model.name).to eq("Places")
        expect(model.game).to eq(new_game)
      end
    end

    context "when the given SynopsisCategory ID doesn't exist" do
      before do
        params = {
          name: "Places",
        }

        put "/api/v1/synopsis_categories/#{synopsis_category.id + 1}", params: params
      end

      it "returns a Not Found response" do
        expect(response).to have_http_status(:not_found)
      end
    end

    context "when the given Game ID doesn't exist" do
      before do
        params = {
          name: "Places",
          game_id: game.id + 1,
        }

        put "/api/v1/synopsis_categories/#{synopsis_category.id}", params: params
      end

      it "returns a Bad Request response" do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          "errors" => {
            "game" => ["must exist"],
          }
        )
      end

      it "doesn't update the SynopsisCategory model in the database" do
        synopsis_category.reload
        expect(synopsis_category.name).to eq("People")
        expect(synopsis_category.game).to eq(game)
      end
    end

    context "when the input violates the model's unique contraints" do
      before do
        create :synopsis_category, name: "Places", game: game
        params = {
          name: "Places",
        }

        put "/api/v1/synopsis_categories/#{synopsis_category.id}", params: params
      end

      it 'returns a Bad Request response with the error message' do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          "errors" => {
            "name" => ["has already been taken"],
          }
        )
      end

      it "doesn't update the SynopsisCategory model in the database" do
        synopsis_category.reload
        expect(synopsis_category.name).to eq("People")
      end
    end

    context "when an id parameter is passed" do
      before do
        params = {
          id: -1,
          name: "Places",
        }

        put "/api/v1/synopsis_categories/#{synopsis_category.id}", params: params
      end

      it 'ignores the id parameter' do
        expect(response).to have_http_status(:no_content)
        expect(synopsis_category.id).not_to eq(-1)
      end
    end
  end

  describe 'DELETE /api/v1/synopsis_categories/:id' do

    let!(:synopsis_category) { create :synopsis_category }

    context 'when the given synopsis_category id exists' do
      before do
        delete "/api/v1/synopsis_categories/#{synopsis_category.id}"
      end

      it "returns a successful response" do
        expect(response).to have_http_status(:no_content)
      end

      it 'removes the SynopsisCategory model from the database' do
        expect(SynopsisCategory.count).to eq(0)
      end
    end

    context "when the given id doesn't exist" do
      before do
        put "/api/v1/synopsis_categories/#{synopsis_category.id + 1}"
      end

      it "returns a Not Found response" do
        expect(response).to have_http_status(:not_found)
      end
    end
  end

end
