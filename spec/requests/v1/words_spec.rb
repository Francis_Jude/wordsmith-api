require 'rails_helper'

RSpec.describe 'Words controller', type: :request do

  describe 'GET /api/v1/characters/:character_id/words' do
    let!(:character) { create :character }
    let!(:word1) { create :word, name: "Inspiring", active: false, character: character }
    let!(:word2) { create :word, name: "Overwhelm", active: true, character: character }

    let!(:other_character) { create :character }
    let!(:irrelevant_word) { create :word, character: other_character }

    it 'returns a list of all words belonging to the given character' do
      get "/api/v1/characters/#{character.id}/words"
      expect(response).to be_success
      expect(json.size).to eq(2)
      assert_response_schema
    end
  end

  describe 'GET /api/v1/words/:id' do

    let!(:word) { create :word }

    context 'when the given word id exists' do
      before do
        get "/api/v1/words/#{word.id}"
      end

      it 'returns the word data in the expected schema' do
        expect(response).to be_success
        assert_response_schema
      end
    end

    context "when the given word id doesn't exist" do
      before do
        get "/api/v1/words/#{word.id + 1}"
      end

      it 'returns a Not Found repsonse' do
        expect(response).to have_http_status(:not_found)
      end
    end
  end

  describe 'POST /api/v1/characters/:character_id/word' do
    context 'when the input is valid' do
      context 'and the optional parameters are supplied' do

        let!(:character) { create :character }

        before do
          params = {
            name: "Inspiring",
            active: true,
          }

          post "/api/v1/characters/#{character.id}/words", params: params
        end

        it "returns a Created response with the new Word model's ID" do
          expect(response).to have_http_status(:created)
          expect(Word.count).to eq(1)
          new_model = Word.first
          expect(json).to eq( 'id' => new_model.id )
        end

        it 'saves a new Word model in the database' do
          expect(Word.count).to eq(1)
          new_model = Word.first
          expect(new_model.name).to eq("Inspiring")
          expect(new_model.active).to be(true)
          expect(new_model.character).to eq(character)
        end
      end

      context 'and the optional parameters are missing' do

        let!(:character) { create :character }

        before do
          params = {
            name: "Inspiring",
          }

          post "/api/v1/characters/#{character.id}/words", params: params
        end

        it "returns a Created response with the new Word model's ID" do
          expect(response).to have_http_status(:created)
          expect(Word.count).to eq(1)
          new_model = Word.first
          expect(json).to eq( 'id' => new_model.id )
        end

        it 'sets default values for the optional properties' do
          expect(Word.count).to eq(1)
          new_model = Word.first
          expect(new_model.active).to be(false)
        end
      end
    end

    context "when the input violates the model's unique contraints" do
      let!(:existing_word) { create :word, name: "Inspiring" }

      before do
        params = {
          name: "Inspiring"
        }

        post "/api/v1/characters/#{existing_word.character.id}/words", params: params
      end

      it 'returns a Bad Request response with the error message' do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          'errors' => {
            'name' => ['has already been taken'],
          }
        )
      end

      it "doesn't save a new Word model" do
        expect(Word.count).to eq(1)
      end
    end

    context 'when required parameters are missing' do
      let!(:character) { create :character }

      before do
        params = {}

        post "/api/v1/characters/#{character.id}/words", params: params
      end

      it 'returns a Bad Request response with the error message' do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          'errors' => {
            'name' => ["can't be blank"],
          }
        )
      end

      it "doesn't save a new Word model" do
        expect(Word.count).to eq(0)
      end
    end

    context "when an id parameter is passed" do
      let!(:character) { create :character }

      before do
        params = {
          id: -1,
          name: "Inspiring",
        }

        post "/api/v1/characters/#{character.id}/words", params: params
      end

      it 'ignores the id parameter' do
        expect(response).to have_http_status(:created)
        expect(Word.count).to eq(1)
        expect(Word.first.id).not_to eq(-1)
      end
    end

    context "when the parent character ID doesn't exist" do
      before do
        params = {
          name: "Inspiring",
        }

        post "/api/v1/characters/1/words", params: params
      end

      it 'returns a Bad Request response with the error message' do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          "errors" => {
            "character" => ["must exist"],
          }
        )
      end
    end
  end

  describe 'PUT/PATCH /api/v1/words/:id' do

    let!(:character) { create :character }
    let!(:word) { create :word, name: "Inspiring", active: true, character: character }

    context 'when the input is valid' do

      let!(:new_character) { create :character }

      before do
        params = {
          name: "Overwhelm",
          active: false,
          character_id: new_character.id,
        }

        put "/api/v1/words/#{word.id}", params: params
      end

      it "returns a successful response" do
        expect(response).to have_http_status(:no_content)
      end

      it 'updates the Word model in the database' do
        expect(Word.count).to eq(1)
        model = Word.first
        expect(model.name).to eq("Overwhelm")
        expect(model.active).to be(false)
        expect(model.character).to eq(new_character)
      end
    end

    context "when the given Word ID doesn't exist" do
      before do
        params = {
          name: "Overwhelm",
        }

        put "/api/v1/words/#{word.id + 1}", params: params
      end

      it "returns a Not Found response" do
        expect(response).to have_http_status(:not_found)
      end
    end

    context "when the given Character ID doesn't exist" do
      before do
        params = {
          name: "Overwhelm",
          active: false,
          character_id: character.id + 1,
        }

        put "/api/v1/words/#{word.id}", params: params
      end

      it "returns a Bad Request response" do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          "errors" => {
            "character" => ["must exist"],
          }
        )
      end

      it "doesn't update the Word model in the database" do
        word.reload
        expect(word.name).to eq("Inspiring")
        expect(word.active).to be(true)
        expect(word.character).to eq(character)
      end
    end

    context "when the input violates the model's unique contraints" do
      before do
        create :word, name: "Overwhelm", character: character
        params = {
          name: "Overwhelm",
        }

        put "/api/v1/words/#{word.id}", params: params
      end

      it 'returns a Bad Request response with the error message' do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          "errors" => {
            "name" => ["has already been taken"],
          }
        )
      end

      it "doesn't update the Word model in the database" do
        word.reload
        expect(word.name).to eq("Inspiring")
      end
    end

    context "when an id parameter is passed" do
      before do
        params = {
          id: -1,
          name: "Overwhelm",
        }

        put "/api/v1/words/#{word.id}", params: params
      end

      it 'ignores the id parameter' do
        expect(response).to have_http_status(:no_content)
        expect(word.id).not_to eq(-1)
      end
    end
  end

  describe 'DELETE /api/v1/words/:id' do

    let!(:word) { create :word }

    context 'when the given word id exists' do
      before do
        delete "/api/v1/words/#{word.id}"
      end

      it "returns a successful response" do
        expect(response).to have_http_status(:no_content)
      end

      it 'removes the Word model from the database' do
        expect(Word.count).to eq(0)
      end
    end

    context "when the given id doesn't exist" do
      before do
        put "/api/v1/words/#{word.id + 1}"
      end

      it "returns a Not Found response" do
        expect(response).to have_http_status(:not_found)
      end
    end
  end

end
