require 'rails_helper'

RSpec.describe 'Items controller', type: :request do

  describe 'GET /api/v1/characters/:character_id/items' do

    let!(:character) { create :character }
    let!(:item1) do
      create(
        :item,
        { name: "Airbrush", description: "Sturdy and reliable.", character: character }
      )
    end
    let!(:item2) do
      create(
        :item,
        { name: "Spray Can", description: "Nearly empty.", character: character }
      )
    end

    let!(:other_character) { create :character }
    let!(:irrelevant_item) { create :item, character: other_character }

    it 'returns a list of all items belonging to the given character' do
      get "/api/v1/characters/#{character.id}/items"
      expect(response).to be_success
      expect(json.size).to eq(2)
      assert_response_schema
    end
  end

  describe 'GET /api/v1/items/:id' do

    let!(:item) { create :item }

    context 'when the given item id exists' do
      before do
        get "/api/v1/items/#{item.id}"
      end

      it 'returns the item data in the expected schema' do
        expect(response).to be_success
        assert_response_schema
      end
    end

    context "when the given item id doesn't exist" do
      before do
        get "/api/v1/items/#{item.id + 1}"
      end

      it 'returns a Not Found repsonse' do
        expect(response).to have_http_status(:not_found)
      end
    end
  end

  describe 'POST /api/v1/characters/:character_id/item' do
    context 'when the input is valid' do
      context 'and the optional parameters are supplied' do

        let!(:character) { create :character }

        before do
          params = {
            name: "Airbrush",
            description: "Sturdy and reliable.",
          }

          post "/api/v1/characters/#{character.id}/items", params: params
        end

        it "returns a Created response with the new Item model's ID" do
          expect(response).to have_http_status(:created)
          expect(Item.count).to eq(1)
          new_model = Item.first
          expect(json).to eq( 'id' => new_model.id )
        end

        it 'saves a new Item model in the database' do
          expect(Item.count).to eq(1)
          new_model = Item.first
          expect(new_model.name).to eq("Airbrush")
          expect(new_model.description).to eq("Sturdy and reliable.")
          expect(new_model.character).to eq(character)
        end
      end

      context 'and the optional parameters are missing' do

        let!(:character) { create :character }

        before do
          params = {
            name: "Airbrush",
            description: "Sturdy and reliable.",
          }

          post "/api/v1/characters/#{character.id}/items", params: params
        end

        it "returns a Created response with the new Item model's ID" do
          expect(response).to have_http_status(:created)
          expect(Item.count).to eq(1)
          new_model = Item.first
          expect(json).to eq( 'id' => new_model.id )
        end
      end
    end

    context 'when required parameters are missing' do
      let!(:character) { create :character }

      before do
        params = {}

        post "/api/v1/characters/#{character.id}/items", params: params
      end

      it 'returns a Bad Request response with the error message' do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          'errors' => {
            'name' => ["can't be blank"],
            'description' => ["can't be blank"],
          }
        )
      end

      it "doesn't save a new Item model" do
        expect(Item.count).to eq(0)
      end
    end

    context "when an id parameter is passed" do
      let!(:character) { create :character }

      before do
        params = {
          id: -1,
          name: "Airbrush",
          description: "Sturdy and reliable.",
        }

        post "/api/v1/characters/#{character.id}/items", params: params
      end

      it 'ignores the id parameter' do
        expect(response).to have_http_status(:created)
        expect(Item.count).to eq(1)
        expect(Item.first.id).not_to eq(-1)
      end
    end

    context "when the parent character ID doesn't exist" do
      before do
        params = {
          name: "Airbrush",
          description: "Sturdy and reliable.",
        }

        post "/api/v1/characters/1/items", params: params
      end

      it 'returns a Bad Request response with the error message' do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          "errors" => {
            "character" => ["must exist"],
          }
        )
      end
    end
  end

  describe 'PUT/PATCH /api/v1/items/:id' do

    let!(:character) { create :character }
    let!(:item) do
      create(
        :item,
        { name: "Airbrush", description: "Sturdy and reliable.", character: character }
      )
    end

    context 'when the input is valid' do

      let!(:new_character) { create :character }

      before do
        params = {
          name: "Spray Can",
          description: "Nearly empty.",
          character_id: new_character.id,
        }

        put "/api/v1/items/#{item.id}", params: params
      end

      it "returns a successful response" do
        expect(response).to have_http_status(:no_content)
      end

      it 'updates the Item model in the database' do
        expect(Item.count).to eq(1)
        model = Item.first
        expect(model.name).to eq("Spray Can")
        expect(model.description).to eq("Nearly empty.")
        expect(model.character).to eq(new_character)
      end
    end

    context "when the given Item ID doesn't exist" do
      before do
        params = {
          name: "Spray Can",
        }

        put "/api/v1/items/#{item.id + 1}", params: params
      end

      it "returns a Not Found response" do
        expect(response).to have_http_status(:not_found)
      end
    end

    context "when the given Character ID doesn't exist" do
      before do
        params = {
          name: "Spray Can",
          description: "Nearly empty.",
          character_id: character.id + 1,
        }

        put "/api/v1/items/#{item.id}", params: params
      end

      it "returns a Bad Request response" do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          "errors" => {
            "character" => ["must exist"],
          }
        )
      end

      it "doesn't update the Item model in the database" do
        item.reload
        expect(item.name).to eq("Airbrush")
        expect(item.description).to eq("Sturdy and reliable.")
        expect(item.character).to eq(character)
      end
    end

    context "when an id parameter is passed" do
      before do
        params = {
          id: -1,
          name: "Spray Can",
        }

        put "/api/v1/items/#{item.id}", params: params
      end

      it 'ignores the id parameter' do
        expect(response).to have_http_status(:no_content)
        expect(item.id).not_to eq(-1)
      end
    end
  end

  describe 'DELETE /api/v1/items/:id' do

    let!(:item) { create :item }

    context 'when the given item id exists' do
      before do
        delete "/api/v1/items/#{item.id}"
      end

      it "returns a successful response" do
        expect(response).to have_http_status(:no_content)
      end

      it 'removes the Item model from the database' do
        expect(Item.count).to eq(0)
      end
    end

    context "when the given id doesn't exist" do
      before do
        put "/api/v1/items/#{item.id + 1}"
      end

      it "returns a Not Found response" do
        expect(response).to have_http_status(:not_found)
      end
    end
  end

end
