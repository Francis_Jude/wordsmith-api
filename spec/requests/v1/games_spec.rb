require 'rails_helper'

RSpec.describe 'Games controller', type: :request do

  describe 'GET /api/v1/games' do
    let!(:game1) { create :game }
    let!(:game2) { create :game }

    it 'returns a list of all games' do
      get '/api/v1/games'
      expect(response).to be_success
      expect(json.size).to eq(2)
      assert_response_schema
    end
  end

  describe 'GET /api/v1/games/:id' do

    let!(:game) { create :game }

    context 'when the given game id exists' do
      before do
        get "/api/v1/games/#{game.id}"
      end

      it 'returns the game data in the expected schema' do
        expect(response).to be_success
        assert_response_schema
      end
    end

    context "when the given game id doesn't exist" do
      before do
        get "/api/v1/games/#{game.id + 1}"
      end

      it 'returns a Not Found repsonse' do
        expect(response).to have_http_status(:not_found)
      end
    end
  end

  describe 'POST /api/v1/game' do

    let(:owner) { create :user }

    context 'when the input is valid' do
      context 'and the optional parameters are supplied' do

        before do
          params = {
            owner_id: owner.id,
            name: "Max's Revel",
            description: 'Max lives up to his name at the tavern.',
            scene: 'The tavern doors swing open, and Max swaggers in...',
            ready: true,
            allow_invites: false,
            visible: false,
          }

          post '/api/v1/games', params: params
        end

        it "returns a Created response with the new Game model's ID" do
          expect(response).to have_http_status(:created)
          expect(Game.count).to eq(1)
          new_model = Game.first
          expect(json).to eq( 'id' => new_model.id )
        end

        it 'saves a new Game model in the database' do
          expect(Game.count).to eq(1)
          new_model = Game.first
          expect(new_model.owner).to eq(owner)
          expect(new_model.name).to eq("Max's Revel")
          expect(new_model.description).to eq('Max lives up to his name at the tavern.')
          expect(new_model.scene).to eq('The tavern doors swing open, and Max swaggers in...')
        end

        it 'sets the optional properties to the given values' do
          expect(Game.count).to eq(1)
          new_model = Game.first
          expect(new_model.ready).to be(true)
          expect(new_model.allow_invites).to be(false)
          expect(new_model.visible).to be(false)
        end
      end

      context 'and the optional parameters are missing' do

        before do
          params = {
            owner_id: owner.id,
            name: "Max's Revel",
            description: 'Max lives up to his name at the tavern.',
            scene: 'The tavern doors swing open, and Max swaggers in...',
          }

          post '/api/v1/games', params: params
        end

        it "returns a Created response with the new Game model's ID" do
          expect(response).to have_http_status(:created)
          expect(Game.count).to eq(1)
          new_model = Game.first
          expect(json).to eq( 'id' => new_model.id )
        end

        it 'saves a new Game model in the database' do
          expect(Game.count).to eq(1)
          new_model = Game.first
          expect(new_model.owner).to eq(owner)
          expect(new_model.name).to eq("Max's Revel")
          expect(new_model.description).to eq('Max lives up to his name at the tavern.')
          expect(new_model.scene).to eq('The tavern doors swing open, and Max swaggers in...')
        end

        it 'sets default values for the optional properties' do
          expect(Game.count).to eq(1)
          new_model = Game.first
          expect(new_model.ready).to be(false)
          expect(new_model.allow_invites).to be(true)
          expect(new_model.visible).to be(true)
        end
      end
    end

    context 'when required parameters are missing' do
      before do
        params = {}

        post '/api/v1/games', params: params
      end

      it 'returns a Bad Request response with the error message' do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          'errors' => {
            'owner' => ['must exist'],
            'name' => ["can't be blank"],
            'description' => ["can't be blank"],
            'scene' => ["can't be blank"],
          }
        )
      end

      it "doesn't save a new Game model" do
        expect(Game.count).to eq(0)
      end
    end
  end

  describe 'PUT/PATCH /api/v1/games/:id' do

    let!(:game) do
      create(
        :game,
        {
          name: "Max's Revel",
          description: 'Max lives up to his name at the tavern.',
          scene: 'The tavern doors swing open, and Max swaggers in...'
        }
      )
    end

    context 'when the input is valid' do

      let!(:new_owner) { create :user }

      before do
        params = {
          owner_id: new_owner.id,
          name: "Max's Hangover",
          description: "After his revel, Max must suffer the consequences.",
          scene: <<~SCENE
            There are beer bottles everywhere. Two of the tavern's tables are
            destroyed. Max awakens with his head pressed against the wooden
            floor. He is clad only in a ripped curtain and some glitter. There
            are cuts and bruises on his arms. One of his eyes is swollen shut.
          SCENE
        }

        put "/api/v1/games/#{game.id}", params: params
      end

      it "returns a successful response" do
        expect(response).to have_http_status(:no_content)
      end

      it 'updates the Game model in the database' do
        expect(Game.count).to eq(1)
        model = Game.first
        expect(model.owner).to eq(new_owner)
        expect(model.name).to eq("Max's Hangover")
        expect(model.description).to eq("After his revel, Max must suffer the consequences.")
        expect(model.scene).to eq(<<~SCENE)
          There are beer bottles everywhere. Two of the tavern's tables are
          destroyed. Max awakens with his head pressed against the wooden
          floor. He is clad only in a ripped curtain and some glitter. There
          are cuts and bruises on his arms. One of his eyes is swollen shut.
        SCENE
      end
    end

    context "when the given id doesn't exist" do

      before do
        params = {}

        put "/api/v1/games/#{game.id + 1}", params: params
      end

      it "returns a Not Found response" do
        expect(response).to have_http_status(:not_found)
      end
    end

    context "when an id parameter is passed" do

      before do
        params = {
          id: -1,
          name: "Max's Revel",
          description: 'Max lives up to his name at the tavern.',
          scene: 'The tavern doors swing open, and Max swaggers in...'
        }

        put "/api/v1/games/#{game.id}", params: params
      end

      it 'ignores the id parameter' do
        expect(response).to have_http_status(:no_content)
        expect(game.id).not_to eq(-1)
      end
    end
  end

  describe 'DELETE /api/v1/games/:id' do

    let!(:game) do
      create(
        :game,
        {
          name: "Max's Revel",
          description: 'Max lives up to his name at the tavern.',
          scene: 'The tavern doors swing open, and Max swaggers in...'
        }
      )
    end

    context 'when the given game id exists' do
      before do
        delete "/api/v1/games/#{game.id}"
      end

      it "returns a successful response" do
        expect(response).to have_http_status(:no_content)
      end

      it 'removes the Game model from the database' do
        expect(Game.count).to eq(0)
      end
    end

    context "when the given id doesn't exist" do
      before do
        put "/api/v1/games/#{game.id + 1}"
      end

      it "returns a Not Found response" do
        expect(response).to have_http_status(:not_found)
      end
    end
  end

end
