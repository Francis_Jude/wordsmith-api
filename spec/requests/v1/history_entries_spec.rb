require 'rails_helper'

RSpec.describe 'HistoryEntries controller', type: :request do

  describe 'GET /api/v1/games/:game_id/history_entries' do
    let!(:game) { create :game }
    let!(:history_entry1) do
      create(
        :history_entry,
        {
          date: Time.utc(2015, 3, 5),
          scene: "The first scene.",
          actions: "The first set of player actions.",
          game: game,
        }
      )
    end
    let!(:history_entry2) do
      create(
        :history_entry,
        {
          date: Time.utc(2015, 4, 5),
          scene: "The second scene.",
          actions: "The second set of player actions.",
          game: game,
        }
      )
    end

    let!(:other_game) { create :game }
    let!(:irrelevant_history_entry) { create :history_entry, game: other_game }

    before { get "/api/v1/games/#{game.id}/history_entries" }

    it 'returns a list of all history_entries belonging to the given game' do
      expect(response).to be_success
      expect(json.size).to eq(2)
      assert_response_schema
    end

    it 'returns the history entries in descending order by date' do
      expect(json.first["scene"]).to eq("The second scene.")
      expect(json.last["scene"]).to eq("The first scene.")
    end

    it 'returns the date properties in ISO-8601 format in UTC' do
      expect(json.first["date"]).to eq("2015-04-05T00:00:00.000Z")
      expect(json.last["date"]).to eq("2015-03-05T00:00:00.000Z")
    end
  end

  describe 'GET /api/v1/history_entries/:id' do

    let!(:history_entry) { create :history_entry, date: Time.utc(1986, 12, 13) }

    context 'when the given history_entry id exists' do
      before do
        get "/api/v1/history_entries/#{history_entry.id}"
      end

      it 'returns the history_entry data in the expected schema' do
        expect(response).to be_success
        assert_response_schema
      end

      it 'returns the date property in ISO-8601 format in UTC' do
        expect(json["date"]).to eq("1986-12-13T00:00:00.000Z")
      end
    end

    context "when the given history_entry id doesn't exist" do
      before do
        get "/api/v1/history_entries/#{history_entry.id + 1}"
      end

      it 'returns a Not Found repsonse' do
        expect(response).to have_http_status(:not_found)
      end
    end
  end

  describe 'POST /api/v1/games/:game_id/history_entry' do
    context 'when the input is valid' do

      let!(:game) { create :game }

      before do
        params = {
          date: "2008-12-24 00:00:00",
          scene: "The scene.",
          actions: "All actions.",
        }

        post "/api/v1/games/#{game.id}/history_entries", params: params
      end

      it "returns a Created response with the new HistoryEntries model's ID" do
        expect(response).to have_http_status(:created)
        expect(HistoryEntry.count).to eq(1)
        new_model = HistoryEntry.first
        expect(json).to eq( 'id' => new_model.id )
      end

      it 'saves a new HistoryEntry model in the database' do
        expect(HistoryEntry.count).to eq(1)
        new_model = HistoryEntry.first
        expect(new_model.date).to eq(Time.utc(2008, 12, 24))
        expect(new_model.scene).to eq("The scene.")
        expect(new_model.actions).to eq("All actions.")
        expect(new_model.game).to eq(game)
      end
    end

    context "when the input violates the model's unique contraints" do
      let!(:existing_history_entry) do
        create :history_entry, date: Time.utc(2008, 12, 31)
      end

      before do
        params = {
          date: "2008-12-31 00:00:00",
          scene: "The scene",
          actions: "All Actions",
        }

        post "/api/v1/games/#{existing_history_entry.game.id}/history_entries", params: params
      end

      it 'returns a Bad Request response with the error message' do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          'errors' => {
            'date' => ['has already been taken'],
          }
        )
      end

      it "doesn't save a new HistoryEntry model" do
        expect(HistoryEntry.count).to eq(1)
      end
    end

    context 'when required parameters are missing' do
      let!(:game) { create :game }

      before do
        params = {}

        post "/api/v1/games/#{game.id}/history_entries", params: params
      end

      it 'returns a Bad Request response with the error message' do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          'errors' => {
            'date' => ["can't be blank"],
            'scene' => ["can't be blank"],
            'actions' => ["can't be blank"],
          }
        )
      end

      it "doesn't save a new HistoryEntry model" do
        expect(HistoryEntry.count).to eq(0)
      end
    end

    context "when the date parameter isn't in ISO-8601" do
      let!(:game) { create :game }

      before do
        params = {
          date: "12/31/2008 00:00:00",
          scene: "The scene",
          actions: "All Actions",
        }

        post "/api/v1/games/#{game.id}/history_entries", params: params
      end

      it 'returns a Bad Request response with the error message' do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          'errors' => {
            'date' => ["can't be blank"],
          }
        )
      end

      it "doesn't save a new HistoryEntry model" do
        expect(HistoryEntry.count).to eq(0)
      end
    end

    context "when an id parameter is passed" do
      let!(:game) { create :game }

      before do
        params = {
          id: -1,
          date: "2008-12-31 00:00:00",
          scene: "The scene",
          actions: "All Actions",
        }

        post "/api/v1/games/#{game.id}/history_entries", params: params
      end

      it 'ignores the id parameter' do
        expect(response).to have_http_status(:created)
        expect(HistoryEntry.count).to eq(1)
        expect(HistoryEntry.first.id).not_to eq(-1)
      end
    end

    context "when the parent game ID doesn't exist" do
      before do
        params = {
          date: "2008-12-31 00:00:00",
          scene: "The scene",
          actions: "All Actions",
        }

        post "/api/v1/games/1/history_entries", params: params
      end

      it 'returns a Bad Request response with the error message' do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          "errors" => {
            "game" => ["must exist"],
          }
        )
      end
    end
  end

  describe 'PUT/PATCH /api/v1/history_entries/:id' do

    let!(:game) { create :game }
    let!(:history_entry) do
      create(
        :history_entry,
        {
          date: Time.utc(2016, 7, 31),
          scene: "The scene.",
          actions: "All actions",
          game: game,
        }
      )
    end

    context 'when the input is valid' do

      let!(:new_game) { create :game }

      before do
        params = {
          date: "2017-07-31 00:00:00",
          scene: "A new scene for a new year.",
          actions: "This year's actions.",
          game_id: new_game.id,
        }

        put "/api/v1/history_entries/#{history_entry.id}", params: params
      end

      it "returns a successful response" do
        expect(response).to have_http_status(:no_content)
      end

      it 'updates the HistoryEntry model in the database' do
        expect(HistoryEntry.count).to eq(1)
        model = HistoryEntry.first
        expect(model.date).to eq(Time.utc(2017, 7, 31))
        expect(model.scene).to eq("A new scene for a new year.")
        expect(model.actions).to eq("This year's actions.")
        expect(model.game).to eq(new_game)
      end
    end

    context "when the given HistoryEntry ID doesn't exist" do
      before do
        params = {
          date: "2017-07-31 00:00:00",
          scene: "A new scene for a new year.",
          actions: "This year's actions.",
          game_id: game.id,
        }

        put "/api/v1/history_entries/#{history_entry.id + 1}", params: params
      end

      it "returns a Not Found response" do
        expect(response).to have_http_status(:not_found)
      end
    end

    context "when the given game ID doesn't exist" do
      before do
        params = {
          date: "2017-07-31 00:00:00",
          scene: "A new scene for a new year.",
          actions: "This year's actions.",
          game_id: game.id + 1,
        }

        put "/api/v1/history_entries/#{history_entry.id}", params: params
      end

      it "returns a Bad Request response" do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          "errors" => {
            "game" => ["must exist"],
          }
        )
      end

      it "doesn't update the HistoryEntry model in the database" do
        history_entry.reload
        expect(history_entry.date).to eq(Time.utc(2016, 7, 31))
        expect(history_entry.scene).to eq("The scene.")
        expect(history_entry.actions).to eq("All actions")
        expect(history_entry.game).to eq(game)
      end
    end

    context "when the input violates the model's unique contraints" do
      before do
        create :history_entry, date: Time.utc(2017, 7, 31), game: game
        params = {
          date: "2017-07-31 00:00:00",
        }

        put "/api/v1/history_entries/#{history_entry.id}", params: params
      end

      it 'returns a Bad Request response with the error message' do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          "errors" => {
            "date" => ["has already been taken"],
          }
        )
      end

      it "doesn't update the HistoryEntry model in the database" do
        history_entry.reload
        expect(history_entry.date).to eq(Time.utc(2016, 7, 31))
      end
    end

    context "when an id parameter is passed" do
      before do
        params = {
          id: -1,
          date: "2017-07-31 00:00:00",
          scene: "A new scene for a new year.",
          actions: "This year's actions.",
          game_id: game.id,
        }

        put "/api/v1/history_entries/#{history_entry.id}", params: params
      end

      it 'ignores the id parameter' do
        expect(response).to have_http_status(:no_content)
        expect(history_entry.id).not_to eq(-1)
      end
    end
  end

  describe 'DELETE /api/v1/history_entries/:id' do

    let!(:history_entry) { create :history_entry }

    context 'when the given history_entry id exists' do
      before do
        delete "/api/v1/history_entries/#{history_entry.id}"
      end

      it "returns a successful response" do
        expect(response).to have_http_status(:no_content)
      end

      it 'removes the HistoryEntry model from the database' do
        expect(HistoryEntry.count).to eq(0)
      end
    end

    context "when the given id doesn't exist" do
      before do
        put "/api/v1/history_entries/#{history_entry.id + 1}"
      end

      it "returns a Not Found response" do
        expect(response).to have_http_status(:not_found)
      end
    end
  end

end
