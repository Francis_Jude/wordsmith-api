require 'rails_helper'

RSpec.describe 'Characters controller', type: :request do

  describe 'GET /api/v1/characters' do
    let!(:character1) { create :character }
    let!(:character2) { create :character }

    it 'returns a list of all characters' do
      get '/api/v1/characters'
      expect(response).to be_success
      expect(json.size).to eq(2)
      assert_response_schema
    end
  end

  describe 'GET /api/v1/characters/:id' do

    let!(:character) { create :character }

    context 'when the given character id exists' do
      before do
        get "/api/v1/characters/#{character.id}"
      end

      it 'returns the character data in the expected schema' do
        expect(response).to be_success
        assert_response_schema
      end
    end

    context "when the given character id doesn't exist" do
      before do
        get "/api/v1/characters/#{character.id + 1}"
      end

      it 'returns a Not Found repsonse' do
        expect(response).to have_http_status(:not_found)
      end
    end
  end

  describe 'POST /api/v1/character' do

    let(:owner) { create :user }
    let(:game) { create :game }

    context 'when the input is valid' do
      context 'and the optional parameters are supplied' do

        before do
          params = {
            owner_id: owner.id,
            game_id: game.id,
            name: "The Golem",
            description: "He's built like a slab of granite. He seems very formidable.",
            action: 'He stands there expressionless.',
            star: true,
            status: "pass",
            roll: "{}",
          }

          post '/api/v1/characters', params: params
        end

        it "returns a Created response with the new Character model's ID" do
          expect(response).to have_http_status(:created)
          expect(Character.count).to eq(1)
          expect(json).to eq( 'id' => Character.first.id )
        end

        it 'saves a new Character model in the database' do
          expect(Character.count).to eq(1)
          new_model = Character.first
          expect(new_model.owner).to eq(owner)
          expect(new_model.game).to eq(game)
          expect(new_model.name).to eq("The Golem")
          expect(new_model.description).to eq(
            "He's built like a slab of granite. He seems very formidable."
          )
        end

        it 'sets the optional properties to the given values' do
          expect(Character.count).to eq(1)
          new_model = Character.first
          expect(new_model.action).to eq("He stands there expressionless.")
          expect(new_model.star).to be(true)
          expect(new_model.status).to eq("pass")
          expect(new_model.roll).to eq("{}")
        end
      end

      context 'and the optional parameters are missing' do

        before do
          params = {
            owner_id: owner.id,
            game_id: game.id,
            name: "The Golem",
            description: "He's built like a slab of granite. He seems very formidable.",
          }

          post '/api/v1/characters', params: params
        end

        it "returns a Created response with the new Character model's ID" do
          expect(response).to have_http_status(:created)
          expect(Character.count).to eq(1)
          expect(json).to eq( 'id' => Character.first.id )
        end

        it 'saves a new Character model in the database' do
          expect(Character.count).to eq(1)
          new_model = Character.first
          expect(new_model.owner).to eq(owner)
          expect(new_model.game).to eq(game)
          expect(new_model.name).to eq("The Golem")
          expect(new_model.description).to eq(
            "He's built like a slab of granite. He seems very formidable."
          )
        end

        it 'sets default values for the optional properties' do
          expect(Character.count).to eq(1)
          new_model = Character.first
          expect(new_model.action).to eq("")
          expect(new_model.star).to be(false)
          expect(new_model.status).to eq("waiting")
          expect(new_model.roll).to be_nil
        end
      end
    end

    context 'when required parameters are missing' do
      before do
        params = {}

        post '/api/v1/characters', params: params
      end

      it 'returns a Bad Request response with the error message' do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          'errors' => {
            'owner' => ['must exist'],
            'game' => ['must exist'],
            'name' => ["can't be blank"],
            'description' => ["can't be blank"],
          }
        )
      end

      it "doesn't save a new Character model" do
        expect(Character.count).to eq(0)
      end
    end
  end

  describe 'PUT/PATCH /api/v1/characters/:id' do

    let(:owner) { create :user }
    let(:game) { create :game }

    let!(:character) do
      create(
        :character,
        {
          owner: owner,
          game: game,
          name: "The Golem",
          description: "He's built like a slab of granite. He seems very formidable.",
          action: 'He stands there expressionless.',
          star: true,
          status: "pass",
          roll: "{}",
        }
      )
    end

    context 'when the input is valid' do

      let!(:other_user) { create :user }
      let!(:other_game) { create :game }

      before do
        params = {
          owner_id: other_user.id,
          game_id: other_game.id,
          name: "Minos",
          description: "He has a soft heart and lonely eyes.",
          action: "He awaits his true love.",
          star: false,
          status: "waiting",
          roll: "{ roll: 8 }",
        }

        put "/api/v1/characters/#{character.id}", params: params
      end

      it "returns a successful response" do
        expect(response).to have_http_status(:no_content)
      end

      it 'updates the Character model in the database' do
        expect(Character.count).to eq(1)
        model = Character.first
        expect(model.name).to eq("Minos")
        expect(model.description).to eq("He has a soft heart and lonely eyes.")
        expect(model.action).to eq("He awaits his true love.")
        expect(model.star).to be(false)
        expect(model.status).to eq("waiting")
        expect(model.roll).to eq("{ roll: 8 }")
      end
    end

    context "when the given id doesn't exist" do

      let!(:other_user) { create :user }
      let!(:other_game) { create :game }

      before do
        params = {
          owner_id: other_user.id,
          game_id: other_game.id,
          name: "Minos",
          description: "He has a soft heart and lonely eyes.",
          action: "He awaits his true love.",
          star: false,
          status: "waiting",
          roll: "{ roll: 8 }",
        }

        put "/api/v1/characters/#{character.id + 1}", params: params
      end

      it "returns a Not Found response" do
        expect(response).to have_http_status(:not_found)
      end
    end

    context "when the input violates the model's unique contraints" do
      before do
        other_user = create :user
        other_game = create :game
        create(
          :character,
          {
            owner: other_user,
            game: other_game,
            name: 'Original Character, Do Not Steal',
          }
        )

        params = {
          owner_id: other_user.id,
          game_id: other_game.id,
          name: 'Original Character, Do Not Steal',
        }

        put "/api/v1/characters/#{character.id}", params: params
      end

      it 'returns a Bad Request response with the error message' do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          'errors' => {
            'owner' => ['has already been taken'],
            'name' => ['has already been taken'],
          }
        )
      end

      it "doesn't update the Character model in the database" do
        character.reload
        expect(character.name).to eq("The Golem")
        expect(character.owner).to eq(owner)
        expect(character.game).to eq(game)
      end
    end

    context "when an id parameter is passed" do

      let!(:other_user) { create :user }
      let!(:other_game) { create :game }

      before do
        params = {
          id: -1,
          owner_id: other_user.id,
          game_id: other_game.id,
          name: "Minos",
          description: "He has a soft heart and lonely eyes.",
          action: "He awaits his true love.",
          star: false,
          status: "waiting",
          roll: "{ roll: 8 }",
        }

        put "/api/v1/characters/#{character.id}", params: params
      end

      it 'ignores the id parameter' do
        expect(response).to have_http_status(:no_content)
        expect(character.id).not_to eq(-1)
      end
    end
  end

  describe 'DELETE /api/v1/characters/:id' do

    let!(:character) { create :character }

    context 'when the given character id exists' do
      before do
        delete "/api/v1/characters/#{character.id}"
      end

      it "returns a successful response" do
        expect(response).to have_http_status(:no_content)
      end

      it 'removes the Character model from the database' do
        expect(Character.count).to eq(0)
      end
    end

    context "when the given id doesn't exist" do
      before do
        put "/api/v1/characters/#{character.id + 1}"
      end

      it "returns a Not Found response" do
        expect(response).to have_http_status(:not_found)
      end
    end
  end

end
