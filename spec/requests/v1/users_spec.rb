require 'rails_helper'

RSpec.describe 'Users controller', type: :request do

  describe 'GET /api/v1/users' do
    let!(:user1) { create :user, name: 'Din', display_name: 'Din1', email: 'din@example.com' }
    let!(:user2) { create :user, name: 'Jin', display_name: 'Jin2', email: 'jin@example.com' }

    it 'returns a list of all users' do
      get '/api/v1/users'
      expect(response).to be_success
      expect(json.size).to eq(2)
      assert_response_schema
    end
  end

  describe 'GET /api/v1/users/:id' do

    let!(:user) { create :user }

    context 'when the given user id exists' do
      before do
        get "/api/v1/users/#{user.id}"
      end

      it 'returns the user data in the expected schema' do
        expect(response).to be_success
        assert_response_schema
      end
    end

    context "when the given user id doesn't exist" do
      before do
        get "/api/v1/users/#{user.id + 1}"
      end

      it 'returns a Not Found repsonse' do
        expect(response).to have_http_status(:not_found)
      end
    end
  end

  describe 'POST /api/v1/user' do
    context 'when the input is valid' do

      before do
        params = {
          name: 'Din',
          display_name: 'Din is the best',
          email: 'din@example.com'
        }

        post '/api/v1/users', params: params
      end

      it "returns a Created response with the new User model's ID" do
        expect(response).to have_http_status(:created)
        expect(User.count).to eq(1)
        new_model = User.first
        expect(json).to eq( 'id' => new_model.id )
      end

      it 'saves a new User model in the database' do
        expect(User.count).to eq(1)
        new_model = User.first
        expect(new_model.name).to eq('Din')
        expect(new_model.display_name).to eq('Din is the best')
        expect(new_model.email).to eq('din@example.com')
      end
    end

    context "when the input violates the model's unique contraints" do
      before do
        create :user, name: 'Din', display_name: 'Din', email: 'din@example.com'
        params = {
          name: 'Jin',
          display_name: 'Din',
          email: 'din@example.com'
        }

        post '/api/v1/users', params: params
      end

      it 'returns a Bad Request response with the error message' do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          'errors' => {
            'display_name' => ['has already been taken'],
            'email' => ['has already been taken'],
          }
        )
      end

      it "doesn't save a new User model" do
        expect(User.count).to eq(1)
      end
    end

    context 'when required parameters are missing' do
      before do
        params = {}

        post '/api/v1/users', params: params
      end

      it 'returns a Bad Request response with the error message' do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          'errors' => {
            'name' => ["can't be blank"],
            'display_name' => ["can't be blank"],
            'email' => ["can't be blank"],
          }
        )
      end

      it "doesn't save a new User model" do
        expect(User.count).to eq(0)
      end
    end

    context "when an id parameter is passed" do
      before do
        params = {
          id: -1,
          name: 'Jin',
          display_name: 'Din',
          email: 'din@example.com'
        }

        post '/api/v1/users', params: params
      end

      it 'ignores the id parameter' do
        expect(response).to have_http_status(:created)
        expect(User.count).to eq(1)
        expect(User.first.id).not_to eq(-1)
      end
    end
  end

  describe 'PUT/PATCH /api/v1/users/:id' do

    let!(:user) do
      create(
        :user,
        name: 'Din', display_name: 'Din is the best', email: 'din@example.com'
      )
    end

    context 'when the input is valid' do
      before do
        params = {
          name: 'Jin',
          display_name: 'Jin is the best',
          email: 'jin@example.com'
        }

        put "/api/v1/users/#{user.id}", params: params
      end

      it "returns a successful response" do
        expect(response).to have_http_status(:no_content)
      end

      it 'updates the User model in the database' do
        expect(User.count).to eq(1)
        model = User.first
        expect(model.name).to eq('Jin')
        expect(model.display_name).to eq('Jin is the best')
        expect(model.email).to eq('jin@example.com')
      end
    end

    context "when the given id doesn't exist" do
      before do
        params = {
          name: 'Jin',
          display_name: 'Jin is the best',
          email: 'jin@example.com'
        }

        put "/api/v1/users/#{user.id + 1}", params: params
      end

      it "returns a Not Found response" do
        expect(response).to have_http_status(:not_found)
      end
    end

    context "when the input violates the model's unique contraints" do
      before do
        create :user, name: 'Jin', display_name: 'Jin', email: 'jin@example.com'
        params = {
          name: 'Jin',
          display_name: 'Jin',
          email: 'jin@example.com'
        }

        put "/api/v1/users/#{user.id}", params: params
      end

      it 'returns a Bad Request response with the error message' do
        expect(response).to have_http_status(:bad_request)
        expect(json).to eq(
          'errors' => {
            'display_name' => ['has already been taken'],
            'email' => ['has already been taken'],
          }
        )
      end

      it "doesn't update the User model in the database" do
        user.reload
        expect(user.name).to eq('Din')
        expect(user.display_name).to eq('Din is the best')
        expect(user.email).to eq('din@example.com')
      end
    end

    context "when an id parameter is passed" do
      before do
        params = {
          id: -1,
          name: 'Jin',
          display_name: 'Din',
          email: 'din@example.com'
        }

        put "/api/v1/users/#{user.id}", params: params
      end

      it 'ignores the id parameter' do
        expect(response).to have_http_status(:no_content)
        expect(user.id).not_to eq(-1)
      end
    end
  end

  describe 'DELETE /api/v1/users/:id' do

    let!(:user) do
      create(
        :user,
        name: 'Din', display_name: 'Din is the best', email: 'din@example.com'
      )
    end

    context 'when the given user id exists' do
      before do
        delete "/api/v1/users/#{user.id}"
      end

      it "returns a successful response" do
        expect(response).to have_http_status(:no_content)
      end

      it 'removes the User model from the database' do
        expect(User.count).to eq(0)
      end
    end

    context "when the given id doesn't exist" do
      before do
        put "/api/v1/users/#{user.id + 1}"
      end

      it "returns a Not Found response" do
        expect(response).to have_http_status(:not_found)
      end
    end
  end

end
