# == Schema Information
#
# Table name: game_bans
#
#  id         :integer          not null, primary key
#  game_id    :integer          not null
#  user_id    :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :game_ban do
    game
    user
  end
end
