# == Schema Information
#
# Table name: synopsis_entries
#
#  id                   :integer          not null, primary key
#  synopsis_category_id :integer          not null
#  name                 :text             not null
#  description          :text             not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

FactoryGirl.define do
  factory :synopsis_entry do
    synopsis_category
    sequence(:name) { |n| "SynopsisEntry #{n}" }
    description "SynopsisEntry Description"
  end
end
