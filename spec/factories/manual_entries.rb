# == Schema Information
#
# Table name: manual_entries
#
#  id         :integer          not null, primary key
#  name       :text             not null
#  content    :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :manual_entry do
    sequence(:name) { |n| "Manual Entry ##{n}" }
    content "ManualEntry Content"
  end
end
