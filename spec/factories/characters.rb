# == Schema Information
#
# Table name: characters
#
#  id          :integer          not null, primary key
#  owner_id    :integer          not null
#  game_id     :integer          not null
#  name        :text             not null
#  description :text             not null
#  action      :text             default(""), not null
#  star        :boolean          default(FALSE), not null
#  status      :integer          default("waiting"), not null
#  roll        :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryGirl.define do
  factory :character do
    association :owner, factory: :user
    game
    sequence(:name) { |n| "Character ##{n}" }
    description "character description"
    action "action"
    star false
    status 0
    roll nil
  end
end
