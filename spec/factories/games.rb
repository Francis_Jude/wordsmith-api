# == Schema Information
#
# Table name: games
#
#  id            :integer          not null, primary key
#  name          :text             not null
#  description   :text             not null
#  scene         :text             not null
#  ready         :boolean          default(FALSE), not null
#  allow_invites :boolean          default(TRUE), not null
#  visible       :boolean          default(TRUE), not null
#  owner_id      :integer          not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

FactoryGirl.define do
  factory :game do
    name "Game Name"
    description "Game Description"
    scene "Game Scene"
    ready false
    allow_invites false
    visible false
    association :owner, factory: :user
  end
end
