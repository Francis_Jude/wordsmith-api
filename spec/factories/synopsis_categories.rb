# == Schema Information
#
# Table name: synopsis_categories
#
#  id         :integer          not null, primary key
#  game_id    :integer          not null
#  name       :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :synopsis_category do
    game
    sequence(:name) { |n| "SynopsisCategory #{n}" }
  end
end
