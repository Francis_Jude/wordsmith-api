# == Schema Information
#
# Table name: users
#
#  id           :integer          not null, primary key
#  name         :text             not null
#  display_name :text             not null
#  email        :text             not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

FactoryGirl.define do
  factory :user do
    name "user name"
    sequence(:display_name) { |n| "user ##{n}" }
    sequence(:email) { |n| "user#{n}@example.com" }
  end
end
