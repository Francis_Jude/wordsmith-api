# == Schema Information
#
# Table name: words
#
#  id           :integer          not null, primary key
#  character_id :integer          not null
#  name         :text             not null
#  active       :boolean          default(FALSE), not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

FactoryGirl.define do
  factory :word do
    sequence(:name) { |n| "Word #{n}" }
    active false
    character
  end
end
