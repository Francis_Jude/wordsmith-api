# == Schema Information
#
# Table name: history_entries
#
#  id         :integer          not null, primary key
#  game_id    :integer          not null
#  date       :datetime         not null
#  actions    :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  scene      :text             not null
#

FactoryGirl.define do
  factory :history_entry do
    game
    date Time.utc(2017, 6, 29, 21, 53, 1)
    scene "scene description"
    actions "all player action text"
  end
end
