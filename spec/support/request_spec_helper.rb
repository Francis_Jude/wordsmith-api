module RequestSpecHelper
  # parse a JSON response into a Ruby hash
  def json
    JSON.parse(response.body)
  end
end
