# Docker

We use Docker Compose to create our production environment. After pulling `wordsmith-api` locally, `cd` into the main project directory that has `docker-compose.yml` and run the following. 

```bash
docker-compose run api rake db:migrate
docker-compose up
```
