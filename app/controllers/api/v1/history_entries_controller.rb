module Api::V1
  class HistoryEntriesController < ApplicationController
    def index
      render json: HistoryEntry.where(game_id: params[:game_id]).order(date: :desc)
    end

    def show
      history_entry = HistoryEntry.find_by(id: params[:id])
      if history_entry.nil?
        head :not_found
      else
        render json: history_entry
      end
    end

    def create
      history_entry = HistoryEntry.create allowed_history_entry_params
      if history_entry.valid?
        if history_entry.save
          render json: { id: history_entry.id }, status: :created
        else
          render json: { errors: history_entry.errors }, status: :internal_server_error
        end
      else
        render json: { errors: history_entry.errors }, status: :bad_request
      end
    end

    def update
      history_entry = HistoryEntry.find_by(id: params[:id])
      if history_entry.nil?
        head :not_found
      else
        if history_entry.update_attributes(allowed_history_entry_params)
          if history_entry.save
            head :no_content
          else
            render json: { errors: history_entry.errors }, status: :internal_server_error
          end
        else
          render json: { errors: history_entry.errors }, status: :bad_request
        end
      end
    end

    def destroy
      history_entry = HistoryEntry.find_by(id: params[:id])
      if history_entry.nil?
        head :not_found
      else
        if history_entry.destroy
          head :no_content
        else
          render json: { errors: history_entry.errors }, status: :internal_server_error
        end
      end
    end

    private

    def allowed_history_entry_params
      params.permit(:date, :scene, :actions, :game_id)
    end
  end
end
