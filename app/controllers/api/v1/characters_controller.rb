module Api::V1
  class CharactersController < ApplicationController
    def index
      render json: Character.all
    end

    def show
      character = Character.find_by(id: params[:id])
      if character.nil?
        head :not_found
      else
        render json: character
      end
    end

    def create
      character = Character.create allowed_character_params

      # unfortunately the 'action' parameter is overwritten by a paramter that
      # Rails always writes into the params hash with the name of the action
      # being invoked, so we have to fetch the action param the user sent via
      # the raw request
      if request.request_parameters['action']
        character.action = request.request_parameters['action']
      end

      if character.valid?
        if character.save
          render json: { id: character.id }, status: :created
        else
          render json: { errors: character.errors }, status: :internal_server_error
        end
      else
        render json: { errors: character.errors }, status: :bad_request
      end
    end

    def update
      character = Character.find_by(id: params[:id])
      if character.nil?
        head :not_found
      else
        character.update_attributes(allowed_character_params)

        # unfortunately the 'action' parameter is overwritten by a paramter that
        # Rails always writes into the params hash with the name of the action
        # being invoked, so we have to fetch the action param the user sent via
        # the raw request
        if request.request_parameters['action']
          character.action = request.request_parameters['action']
        end

        if character.valid?
          if character.save
            head :no_content
          else
            render json: { errors: character.errors }, status: :internal_server_error
          end
        else
          render json: { errors: character.errors }, status: :bad_request
        end
      end
    end

    def destroy
      character = Character.find_by(id: params[:id])
      if character.nil?
        head :not_found
      else
        if character.destroy
          head :no_content
        else
          render json: { errors: character.errors }, status: :internal_server_error
        end
      end
    end

    private

    def allowed_character_params
      params.permit(
        :name, :description, :star, :status, :roll, :owner_id, :game_id
      )
    end
  end
end
