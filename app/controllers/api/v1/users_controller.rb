module Api::V1
  class UsersController < ApplicationController
    def index
      render json: User.all
    end

    def show
      user = User.find_by(id: params[:id])
      if user.nil?
        head :not_found
      else
        render json: user
      end
    end

    def create
      user = User.create allowed_user_params
      if user.valid?
        if user.save
          render json: { id: user.id }, status: :created
        else
          render json: { errors: user.errors }, status: :internal_server_error
        end
      else
        render json: { errors: user.errors }, status: :bad_request
      end
    end

    def update
      user = User.find_by(id: params[:id])
      if user.nil?
        head :not_found
      else
        if user.update_attributes(allowed_user_params)
          if user.save
            head :no_content
          else
            render json: { errors: user.errors }, status: :internal_server_error
          end
        else
          render json: { errors: user.errors }, status: :bad_request
        end
      end
    end

    def destroy
      user = User.find_by(id: params[:id])
      if user.nil?
        head :not_found
      else
        if user.destroy
          head :no_content
        else
          render json: { errors: user.errors }, status: :internal_server_error
        end
      end
    end

    private

    def allowed_user_params
      params.permit(:name, :display_name, :email)
    end
  end
end
