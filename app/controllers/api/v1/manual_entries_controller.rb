module Api::V1
  class ManualEntriesController < ApplicationController
    def index
      render json: ManualEntry.all
    end

    def show
      manual_entry = ManualEntry.find_by(id: params[:id])
      if manual_entry.nil?
        head :not_found
      else
        render json: manual_entry
      end
    end

    def create
      manual_entry = ManualEntry.create allowed_manual_entry_params
      if manual_entry.valid?
        if manual_entry.save
          render json: { id: manual_entry.id }, status: :created
        else
          render json: { errors: manual_entry.errors }, status: :internal_server_error
        end
      else
        render json: { errors: manual_entry.errors }, status: :bad_request
      end
    end

    def update
      manual_entry = ManualEntry.find_by(id: params[:id])
      if manual_entry.nil?
        head :not_found
      else
        if manual_entry.update_attributes(allowed_manual_entry_params)
          if manual_entry.save
            head :no_content
          else
            render json: { errors: manual_entry.errors }, status: :internal_server_error
          end
        else
          render json: { errors: manual_entry.errors }, status: :bad_request
        end
      end
    end

    def destroy
      manual_entry = ManualEntry.find_by(id: params[:id])
      if manual_entry.nil?
        head :not_found
      else
        if manual_entry.destroy
          head :no_content
        else
          render json: { errors: manual_entry.errors }, status: :internal_server_error
        end
      end
    end

    private

    def allowed_manual_entry_params
      params.permit(
        :name, :content
      )
    end
  end
end
