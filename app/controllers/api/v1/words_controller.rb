module Api::V1
  class WordsController < ApplicationController
    def index
      render json: Word.where(character_id: params[:character_id]).all
    end

    def show
      word = Word.find_by(id: params[:id])
      if word.nil?
        head :not_found
      else
        render json: word
      end
    end

    def create
      word = Word.create allowed_word_params
      if word.valid?
        if word.save
          render json: { id: word.id }, status: :created
        else
          render json: { errors: word.errors }, status: :internal_server_error
        end
      else
        render json: { errors: word.errors }, status: :bad_request
      end
    end

    def update
      word = Word.find_by(id: params[:id])
      if word.nil?
        head :not_found
      else
        if word.update_attributes(allowed_word_params)
          if word.save
            head :no_content
          else
            render json: { errors: word.errors }, status: :internal_server_error
          end
        else
          render json: { errors: word.errors }, status: :bad_request
        end
      end
    end

    def destroy
      word = Word.find_by(id: params[:id])
      if word.nil?
        head :not_found
      else
        if word.destroy
          head :no_content
        else
          render json: { errors: word.errors }, status: :internal_server_error
        end
      end
    end

    private

    def allowed_word_params
      params.permit(:name, :active, :character_id)
    end
  end
end
