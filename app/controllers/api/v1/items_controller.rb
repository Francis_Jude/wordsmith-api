module Api::V1
  class ItemsController < ApplicationController
    def index
      render json: Item.where(character_id: params[:character_id]).all
    end

    def show
      item = Item.find_by(id: params[:id])
      if item.nil?
        head :not_found
      else
        render json: item
      end
    end

    def create
      item = Item.create allowed_item_params
      if item.valid?
        if item.save
          render json: { id: item.id }, status: :created
        else
          render json: { errors: item.errors }, status: :internal_server_error
        end
      else
        render json: { errors: item.errors }, status: :bad_request
      end
    end

    def update
      item = Item.find_by(id: params[:id])
      if item.nil?
        head :not_found
      else
        if item.update_attributes(allowed_item_params)
          if item.save
            head :no_content
          else
            render json: { errors: item.errors }, status: :internal_server_error
          end
        else
          render json: { errors: item.errors }, status: :bad_request
        end
      end
    end

    def destroy
      item = Item.find_by(id: params[:id])
      if item.nil?
        head :not_found
      else
        if item.destroy
          head :no_content
        else
          render json: { errors: item.errors }, status: :internal_server_error
        end
      end
    end

    private

    def allowed_item_params
      params.permit(:name, :description, :character_id)
    end
  end
end
