module Api::V1
  class SynopsisCategoriesController < ApplicationController
    def index
      render json: SynopsisCategory.where(game_id: params[:game_id]).all
    end

    def show
      synopsis_category = SynopsisCategory.find_by(id: params[:id])
      if synopsis_category.nil?
        head :not_found
      else
        render json: synopsis_category
      end
    end

    def create
      synopsis_category = SynopsisCategory.create allowed_synopsis_category_params
      if synopsis_category.valid?
        if synopsis_category.save
          render json: { id: synopsis_category.id }, status: :created
        else
          render json: { errors: synopsis_category.errors }, status: :internal_server_error
        end
      else
        render json: { errors: synopsis_category.errors }, status: :bad_request
      end
    end

    def update
      synopsis_category = SynopsisCategory.find_by(id: params[:id])
      if synopsis_category.nil?
        head :not_found
      else
        if synopsis_category.update_attributes(allowed_synopsis_category_params)
          if synopsis_category.save
            head :no_content
          else
            render json: { errors: synopsis_category.errors }, status: :internal_server_error
          end
        else
          render json: { errors: synopsis_category.errors }, status: :bad_request
        end
      end
    end

    def destroy
      synopsis_category = SynopsisCategory.find_by(id: params[:id])
      if synopsis_category.nil?
        head :not_found
      else
        if synopsis_category.destroy
          head :no_content
        else
          render json: { errors: synopsis_category.errors }, status: :internal_server_error
        end
      end
    end

    private

    def allowed_synopsis_category_params
      params.permit(:name, :game_id)
    end
  end
end
