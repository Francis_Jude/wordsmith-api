module Api::V1
  class SynopsisEntriesController < ApplicationController
    def index
      render json: SynopsisEntry.where(synopsis_category_id: params[:synopsis_category_id]).all
    end

    def show
      synopsis_entry = SynopsisEntry.find_by(id: params[:id])
      if synopsis_entry.nil?
        head :not_found
      else
        render json: synopsis_entry
      end
    end

    def create
      synopsis_entry = SynopsisEntry.create allowed_synopsis_entry_params
      if synopsis_entry.valid?
        if synopsis_entry.save
          render json: { id: synopsis_entry.id }, status: :created
        else
          render json: { errors: synopsis_entry.errors }, status: :internal_server_error
        end
      else
        render json: { errors: synopsis_entry.errors }, status: :bad_request
      end
    end

    def update
      synopsis_entry = SynopsisEntry.find_by(id: params[:id])
      if synopsis_entry.nil?
        head :not_found
      else
        if synopsis_entry.update_attributes(allowed_synopsis_entry_params)
          if synopsis_entry.save
            head :no_content
          else
            render json: { errors: synopsis_entry.errors }, status: :internal_server_error
          end
        else
          render json: { errors: synopsis_entry.errors }, status: :bad_request
        end
      end
    end

    def destroy
      synopsis_entry = SynopsisEntry.find_by(id: params[:id])
      if synopsis_entry.nil?
        head :not_found
      else
        if synopsis_entry.destroy
          head :no_content
        else
          render json: { errors: synopsis_entry.errors }, status: :internal_server_error
        end
      end
    end

    private

    def allowed_synopsis_entry_params
      params.permit(:name, :description, :synopsis_category_id)
    end
  end
end
