module Api::V1
  class GamesController < ApplicationController
    def index
      render json: Game.all
    end

    def show
      game = Game.find_by(id: params[:id])
      if game.nil?
        head :not_found
      else
        render json: game
      end
    end

    def create
      game = Game.create allowed_game_params
      if game.valid?
        if game.save
          render json: { id: game.id }, status: :created
        else
          render json: { errors: game.errors }, status: :internal_server_error
        end
      else
        render json: { errors: game.errors }, status: :bad_request
      end
    end

    def update
      game = Game.find_by(id: params[:id])
      if game.nil?
        head :not_found
      else
        if game.update_attributes(allowed_game_params)
          if game.save
            head :no_content
          else
            render json: { errors: game.errors }, status: :internal_server_error
          end
        else
          render json: { errors: game.errors }, status: :bad_request
        end
      end
    end

    def destroy
      game = Game.find_by(id: params[:id])
      if game.nil?
        head :not_found
      else
        if game.destroy
          head :no_content
        else
          render json: { errors: game.errors }, status: :internal_server_error
        end
      end
    end

    private

    def allowed_game_params
      params.permit(
        :name, :description, :scene, :owner_id, :ready, :allow_invites, :visible
      )
    end
  end
end
