# == Schema Information
#
# Table name: games
#
#  id            :integer          not null, primary key
#  name          :text             not null
#  description   :text             not null
#  scene         :text             not null
#  ready         :boolean          default(FALSE), not null
#  allow_invites :boolean          default(TRUE), not null
#  visible       :boolean          default(TRUE), not null
#  owner_id      :integer          not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class GameSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :scene, :owner_id

  has_many :invited_users
  has_many :banned_users
  has_many :characters

  class UserSerializer < ActiveModel::Serializer
    attributes :id
  end

  class CharacterSerializer < ActiveModel::Serializer
    attributes :id
  end
end
