# == Schema Information
#
# Table name: characters
#
#  id          :integer          not null, primary key
#  owner_id    :integer          not null
#  game_id     :integer          not null
#  name        :text             not null
#  description :text             not null
#  action      :text             default(""), not null
#  star        :boolean          default(FALSE), not null
#  status      :integer          default("waiting"), not null
#  roll        :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class CharacterSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :action, :star, :status, :roll,
    :game_id, :owner_id

  has_many :words
  has_many :items

  class WordSerializer < ActiveModel::Serializer
    attributes :id
  end

  class ItemSerializer < ActiveModel::Serializer
    attributes :id
  end
end
