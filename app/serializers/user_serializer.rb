# == Schema Information
#
# Table name: users
#
#  id           :integer          not null, primary key
#  name         :text             not null
#  display_name :text             not null
#  email        :text             not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class UserSerializer < ActiveModel::Serializer
  attributes :id, :name, :display_name, :email

  has_many :owned_games
  has_many :games_invited_to
  has_many :banned_games
  has_many :characters

  class GameSerializer < ActiveModel::Serializer
    attributes :id
  end

  class CharacterSerializer < ActiveModel::Serializer
    attributes :id
  end
end
