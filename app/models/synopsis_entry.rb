# == Schema Information
#
# Table name: synopsis_entries
#
#  id                   :integer          not null, primary key
#  synopsis_category_id :integer          not null
#  name                 :text             not null
#  description          :text             not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class SynopsisEntry < ApplicationRecord
  validates :name, :description, presence: true
  validates :name, uniqueness: { scope: :synopsis_category_id }

  belongs_to :synopsis_category
end
