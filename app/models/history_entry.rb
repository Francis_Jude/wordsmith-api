# == Schema Information
#
# Table name: history_entries
#
#  id         :integer          not null, primary key
#  game_id    :integer          not null
#  date       :datetime         not null
#  actions    :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  scene      :text             not null
#

class HistoryEntry < ApplicationRecord
  validates :date, :scene, :actions, presence: true
  validates :date, uniqueness: { scope: :game_id }

  belongs_to :game
end
