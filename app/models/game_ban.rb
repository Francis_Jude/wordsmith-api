# == Schema Information
#
# Table name: game_bans
#
#  id         :integer          not null, primary key
#  game_id    :integer          not null
#  user_id    :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class GameBan < ApplicationRecord
  validates :user, uniqueness: { scope: :game_id }

  belongs_to :user
  belongs_to :game
end
