# == Schema Information
#
# Table name: users
#
#  id           :integer          not null, primary key
#  name         :text             not null
#  display_name :text             not null
#  email        :text             not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class User < ApplicationRecord
  validates :name, :display_name, :email, presence: true
  validates :display_name, :email, uniqueness: true

  has_many :owned_games, class_name: 'Game', inverse_of: 'owner', foreign_key: 'owner_id'
  has_many :game_invites, inverse_of: :user
  has_many :game_bans, inverse_of: :user
  has_many :games_invited_to, through: :game_invites, class_name: 'Game', source: :game
  has_many :banned_games, through: :game_bans, class_name: 'Game', source: :game
  has_many :characters, inverse_of: 'owner', foreign_key: 'owner_id'
end
