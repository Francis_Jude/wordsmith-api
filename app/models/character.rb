# == Schema Information
#
# Table name: characters
#
#  id          :integer          not null, primary key
#  owner_id    :integer          not null
#  game_id     :integer          not null
#  name        :text             not null
#  description :text             not null
#  action      :text             default(""), not null
#  star        :boolean          default(FALSE), not null
#  status      :integer          default("waiting"), not null
#  roll        :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Character < ApplicationRecord
  validates :name, :description, presence: true
  validates :owner, uniqueness: { scope: :game_id }
  validates :name, uniqueness: { scope: :game_id }

  belongs_to :owner, class_name: 'User'
  belongs_to :game
  has_many :words, inverse_of: :character
  has_many :items, inverse_of: :character

  enum status: [:waiting, :ready, :pass]
end
