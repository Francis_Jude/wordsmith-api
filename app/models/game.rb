# == Schema Information
#
# Table name: games
#
#  id            :integer          not null, primary key
#  name          :text             not null
#  description   :text             not null
#  scene         :text             not null
#  ready         :boolean          default(FALSE), not null
#  allow_invites :boolean          default(TRUE), not null
#  visible       :boolean          default(TRUE), not null
#  owner_id      :integer          not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Game < ApplicationRecord
  validates :name, :description, :scene, presence: true

  belongs_to :owner, class_name: 'User'
  has_many :game_invites, inverse_of: :game
  has_many :game_bans, inverse_of: :game
  has_many :invited_users, through: :game_invites, class_name: 'User', source: :user
  has_many :banned_users, through: :game_bans, class_name: 'User', source: :user
  has_many :characters, inverse_of: :game
  has_many :history_entries, inverse_of: :game
  has_many :synopsis_items, inverse_of: :game
end
