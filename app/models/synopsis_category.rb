# == Schema Information
#
# Table name: synopsis_categories
#
#  id         :integer          not null, primary key
#  game_id    :integer          not null
#  name       :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class SynopsisCategory < ApplicationRecord
  validates :name, presence: true
  validates :name, uniqueness: { scope: :game_id }

  belongs_to :game
  has_many :synopsis_entries, inverse_of: :synopsis_category
end
