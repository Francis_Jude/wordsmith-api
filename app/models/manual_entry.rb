# == Schema Information
#
# Table name: manual_entries
#
#  id         :integer          not null, primary key
#  name       :text             not null
#  content    :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ManualEntry < ApplicationRecord
  validates :name, :content, presence: true
  validates :name, uniqueness: true
end
