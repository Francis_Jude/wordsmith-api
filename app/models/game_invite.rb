# == Schema Information
#
# Table name: game_invites
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  game_id    :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class GameInvite < ApplicationRecord
  validates :user, uniqueness: { scope: :game_id }

  belongs_to :user
  belongs_to :game
end
