# == Schema Information
#
# Table name: words
#
#  id           :integer          not null, primary key
#  character_id :integer          not null
#  name         :text             not null
#  active       :boolean          default(FALSE), not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Word < ApplicationRecord
  validates_presence_of :name, presence: true
  validates :name, uniqueness: { scope: :character_id }

  belongs_to :character
end
