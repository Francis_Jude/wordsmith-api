# == Schema Information
#
# Table name: items
#
#  id           :integer          not null, primary key
#  character_id :integer          not null
#  name         :text             not null
#  description  :text             not null
#  active       :boolean          default(FALSE), not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Item < ApplicationRecord
  validates_presence_of :name, :description, presence: true

  belongs_to :character
end
