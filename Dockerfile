# https://docs.docker.com/compose/rails/#define-the-project
FROM ruby:latest
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /wordsmith-api
WORKDIR /wordsmith-api
ADD . /wordsmith-api
RUN bundle install
