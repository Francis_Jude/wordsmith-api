Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :users

      resources :games, shallow: true do
        resources :history_entries
        resources :synopsis_categories do
          resources :synopsis_entries, shallow: true
        end
      end

      resources :characters, shallow: true do
        resources :words
        resources :items
      end

      resources :manual_entries
    end
  end
end
